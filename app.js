$(window).on('load',function(){
    $("#preloader").delay(5000).fadeOut('slow');
});




$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
      items:3,
      autoplay:true,
      margin:20,
      nav:false,
      loop:true,
      smartSpeed:2000,
      autoplayHoverPause:true,
      dots:true,
      navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i>']
  });
});


$(document).ready(function(){
    $("#work").magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function(openerElement){
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        }
    })
});



